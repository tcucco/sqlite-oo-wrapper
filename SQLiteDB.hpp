#ifndef SQLITE_DB_HPP
#define SQLITE_DB_HPP

#include <string>
#include <iterator>
#include <functional>

// Third Party Forward Declarations
struct sqlite3;
struct sqlite3_stmt;

namespace Data
{

using std::string;

// Forward Declarations
class DataException;
class SQLiteConnection;
class SQLiteCursor;
template<class T> class SQLiteDataReader;


class DataException : public std::exception
{
public:
	DataException(string message);
	virtual ~DataException() throw();

	inline virtual const char* what() const throw() { return _message.c_str(); }

private:
	string _message;	
}; // class DataException


class SqlNull { };


class SQLiteConnection
{
public:
  SQLiteConnection(string connectionString);
  ~SQLiteConnection();

  inline bool opened() const { return _opened; }
  
  int affectedRowCount() const;
  long lastInsertRowID() const;
  const char* errorMessage() const;

  SQLiteConnection& open();
  SQLiteConnection& close();
  SQLiteConnection& execute(const string& command);
  
  SQLiteCursor cursor(const string& command);
  
private:
  bool _opened;
  string _connectionString;
  sqlite3* _conn;
  
  friend class SQLiteCursor;
}; // class SQLiteConnection


class SQLiteCursor
{
public:
  SQLiteCursor(const SQLiteConnection& conn, const string& command);
  ~SQLiteCursor();
  
  inline int affectedRowCount() const { return _affectedRowCount; }
  inline long lastInsertRowID() const { return _lastInsertRowId; }
  
  int getParameterIndex(const string& parameterName);
  string getParameterName(int index);
  
  SQLiteCursor& bind(int i, int v);
  SQLiteCursor& bind(int i, bool v);
  SQLiteCursor& bind(int i, long v);
  SQLiteCursor& bind(int i, double v);
  SQLiteCursor& bind(int i, const string& v);
  SQLiteCursor& bind(int i, SqlNull& v);
  SQLiteCursor& bind(int i, const void* blob, int bytes);
  SQLiteCursor& bind(int i, const void* blob, int bytes, void(*dispose)(void*));
  
  // template<class T>
  // SQLiteDataReader<T> dataReader(SQLiteDataReader<T>::Extractor extractor);
  
  bool next();
  
  int columnCount() const;
  string columnTableName(int i) const;
  string columnName(int i) const;
  
  bool isNull(int i) const;
    
  int getInt(int i) const;
  bool getBool(int i) const;
  long getLong(int i) const;
  double getDouble(int i) const;
  string getString(int i) const;
  
  // template<class T> 
  // SQLiteDataReader<T> dataReader(typename SQLiteDataReader<T>::Extractor extractor)
  // {
  //   return SQLiteDataReader<T>(*this, extractor);
  // }

private:
  int _affectedRowCount;
  long _lastInsertRowId;
  sqlite3_stmt* _stmt;
  const string& _command;
  const SQLiteConnection& _conn;
}; // class SQLiteCursor


template<class T>
class SQLiteDataReader
{
public:
  //typedef T(*Extractor)(SQLiteCursor&);
  typedef std::function<T (SQLiteCursor&)> Extractor;
  
  SQLiteDataReader(SQLiteCursor& cursor, Extractor extractor)
    : _cursor(cursor), _extractor(extractor), _recordNumber(0)
  { }
  
  inline T& current() { return _current; }
  inline int recordNumber() { return _recordNumber; }

  bool next()
  {
    if (_cursor.next())
    {
      _current = _extractor(_cursor);
      ++_recordNumber;
      return true;
    }
    else
    {
      _recordNumber = -1;
      return false;
    }
  }

private:
  SQLiteCursor& _cursor;
  Extractor _extractor;
  T _current;
  int _recordNumber;
};


} // namespace Data

#endif //SQLITE_DB_HPP
