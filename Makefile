CXX = /Applications/GCC/4.6.3/bin/g++
CXXFLAGS = -std=c++0x -Wall -pedantic -ggdb
OBJECTS = obj/main.o lib/libSQLiteDB.a
OUTPUT = test.out

all: $(OBJECTS)
	$(CXX) -o $(OUTPUT) $(OBJECTS)

obj/main.o: main.cpp
	$(CXX) $(CXXFLAGS) -c main.cpp -o obj/main.o

# By joining sqlite3.o and SQLiteDB.o into an archive, I can use this library
# in other projects by simply having the SQLiteDB.hpp and libSQLiteDB.a files.
# I've set up SQLiteDB.hpp so that it doesn't need a reference to sqlite3.h, it
# instead uses forward declarations.
lib/libSQLiteDB.a: obj/SQLiteDB.o
	cd SQLite; make;
	ar rcs lib/libSQLiteDB.a obj/SQLiteDB.o SQLite/sqlite.o

obj/SQLiteDB.o: SQLiteDB.cpp SQLiteDB.hpp
	cd SQLite; make headers;
	$(CXX) $(CXXFLAGS) -c SQLiteDB.cpp -o obj/SQLiteDB.o

clean:
	rm obj/* lib/* $(OUTPUT)
	cd SQLite; make clean;
