This is meant to be a simple OO wrapper on SQLite. It's not a complete
representation of all the abilities of SQLite, but it covers the basics for
what I generally use. I'll continue to develop it as I have needs that aren't
met by it.

There are other excellent OO wrappers on SQLite3 out there already. I
primarily started this as an exercise to learn the C api of SQLite better, and
to work on my C++. I've found it very useful since then.

The SQLite directory is setup to hold various versions of SQLite, and the
makefile in the SQLite directory acts as a switchboard. Set the VER variable,
and the makefile will make sure that the appropriate headers and object files
are where they need to be when the time comes.

The principal output of the program is lib/libSQLiteDB.a. This archive bundles
together the SQLiteDB object code and the SQLite object code. The header for
SQLiteDB is set up in such a way that a user needs only have the lib file and
the header file to use in a project. They will not need sqlite3.o or sqlite3.h.

Also, please note that the Makefile for this project may seem barbaric. I've
also been using this project to learn more about make.
