#include <exception>
#include <string>
#include "SQLite/sqlite.h"
#include "SQLiteDB.hpp"


namespace Data
{

using std::string;

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Hidden Helper Function Declarations
void checkBinding(int rc, const SQLiteConnection& conn);


//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Data Exception Definitions
DataException::DataException(string message)
  : _message(message)
{ }

DataException::~DataException() throw()
{ }


//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// SQLiteConnection Definitions
SQLiteConnection::SQLiteConnection(string connectionString)
  : _opened(false), _connectionString(connectionString)
{ }

SQLiteConnection::~SQLiteConnection()
{
  if (opened())
  {
    close();
  }
}

int SQLiteConnection::affectedRowCount() const { return sqlite3_changes(_conn); }

long SQLiteConnection::lastInsertRowID() const { return sqlite3_last_insert_rowid(_conn); }

const char* SQLiteConnection::errorMessage() const { return sqlite3_errmsg(_conn); }

SQLiteConnection& SQLiteConnection::open()
{
  if (opened())
  {
    throw DataException("Already connected to database.");
  }
  
  int rc = sqlite3_open(_connectionString.c_str(), &_conn);
  
  if (rc != SQLITE_OK)
  {
    DataException de = DataException(string("Error connecting to database: ") + errorMessage());
    close();
    throw de;
  }
  
  _opened = true;
  return *this;
}

SQLiteConnection& SQLiteConnection::close()
{
  sqlite3_close(_conn);
  _conn = 0;
  _opened = false;
  return *this;
}

SQLiteConnection& SQLiteConnection::execute(const string& command)
{
  cursor(command).next();
  return *this;
}
  
SQLiteCursor SQLiteConnection::cursor(const string& command)
{
  return SQLiteCursor(*this, command);
}


//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// SQLiteCursor Definitions

SQLiteCursor::SQLiteCursor(const SQLiteConnection& conn, const string& command)
  : _command(command), _conn(conn)
{
  int rc = sqlite3_prepare_v2(conn._conn, command.c_str(), -1, &_stmt, 0);
  
  if (rc != SQLITE_OK)
  {
    throw DataException(string("An error occurred when preparing the statement: ") + conn.errorMessage());
  }
}

SQLiteCursor::~SQLiteCursor()
{
  sqlite3_finalize(_stmt);
  _stmt = 0;
}

int SQLiteCursor::getParameterIndex(const string& parameterName)
{
  return sqlite3_bind_parameter_index(_stmt, parameterName.c_str());
}

string SQLiteCursor::getParameterName(int index)
{
  const char* name = sqlite3_bind_parameter_name(_stmt, index);
  
  return name != 0 ? string(name) : string();
}

SQLiteCursor& SQLiteCursor::bind(int i, bool v)
{
  checkBinding(sqlite3_bind_int(_stmt, i, v ? 1 : 0), _conn);
  return *this;
}

SQLiteCursor& SQLiteCursor::bind(int i, int v)
{
  checkBinding(sqlite3_bind_int(_stmt, i, v), _conn);
  return *this;
}

SQLiteCursor& SQLiteCursor::bind(int i, long v)
{
  checkBinding(sqlite3_bind_int64(_stmt, i, v), _conn);
  return *this;  
}

SQLiteCursor& SQLiteCursor::bind(int i, double v)
{
  checkBinding(sqlite3_bind_double(_stmt, i, v), _conn);
  return *this;
}

SQLiteCursor& SQLiteCursor::bind(int i, const string& v)
{
  checkBinding(sqlite3_bind_text(_stmt, i, v.c_str(), -1, SQLITE_STATIC), _conn);
  return *this;  
}

SQLiteCursor& SQLiteCursor::bind(int i, SqlNull& v)
{
  checkBinding(sqlite3_bind_null(_stmt, i), _conn);
  return *this;  
}

SQLiteCursor& SQLiteCursor::bind(int i, const void* blob, int bytes)
{
  return bind(i, blob, bytes, SQLITE_STATIC);
}

SQLiteCursor& SQLiteCursor::bind(int i, const void* blob, int bytes, void(*dispose)(void*))
{
  checkBinding(sqlite3_bind_blob(_stmt, i, blob, bytes, dispose), _conn);
  return *this;
}
  
bool SQLiteCursor::next()
{
  int rc = sqlite3_step(_stmt);
  
  if (rc == SQLITE_ROW)
  {
    return true;
  }
  else if (rc == SQLITE_DONE)
  {
    _lastInsertRowId = _conn.lastInsertRowID();
    _affectedRowCount = _conn.affectedRowCount();
    return false;
  }
  else
  {
    throw DataException(string("An error occurred when executing the statement: ") + _conn.errorMessage());
  }
}

int SQLiteCursor::columnCount() const
{
  return sqlite3_column_count(_stmt);
}

#ifdef SQLITE_ENABLE_COLUMN_METADATA
string SQLiteCursor::columnTableName(int i) const
{
  const char* name = sqlite3_column_table_name(_stmt, i);
  
  return name != 0 ? string(name) : string();
}

string SQLiteCursor::columnName(int i) const
{
  const char* name = sqlite3_column_origin_name(_stmt, i);
  
  return name != 0 ? string(name) : string();
}
#endif

bool SQLiteCursor::isNull(int i) const
{
  return sqlite3_column_type(_stmt, i) == SQLITE_NULL;
}

int SQLiteCursor::getInt(int i) const
{
  return sqlite3_column_int(_stmt, i);
}

bool SQLiteCursor::getBool(int i) const
{
  return sqlite3_column_int(_stmt, i) != 0;
}

long SQLiteCursor::getLong(int i) const
{
  return sqlite3_column_int64(_stmt, i);
}

double SQLiteCursor::getDouble(int i) const
{
  return sqlite3_column_double(_stmt, i);
}

string SQLiteCursor::getString(int i) const
{
  return string((char*)sqlite3_column_text(_stmt, i));
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// SQLiteCursor Helpers
void checkBinding(int rc, const SQLiteConnection& conn)
{
  if (rc != SQLITE_OK) { throw DataException(string("An error occurred when binding: ") + conn.errorMessage()); }
}


} // namespace Data
