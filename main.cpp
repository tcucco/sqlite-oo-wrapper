#include <iostream>
#include "SQLiteDB.hpp"

using std::string;
using std::ostream;

struct SQLiteMaster
{
  SQLiteMaster() { }
  SQLiteMaster(string t, string n, string tn, int rp)
    : type(t), name(n), tbl_name(tn), rootpage(rp)
  { }
  
  string type;
  string name;
  string tbl_name;
  int rootpage;
};

ostream& operator<<(ostream& s, const SQLiteMaster& m)
{
  using std::endl;
  
  s << "Type:      " << m.type << endl
    << "Name:      " << m.name << endl
    << "Tbl Name:  " << m.tbl_name << endl
    << "Root Page: " << m.rootpage << endl;
    
  return s;
}

SQLiteMaster fromCursor(Data::SQLiteCursor& cr)
{
  string t = cr.isNull(0) ? string("NULL") : cr.getString(0);
  string n = cr.isNull(1) ? string("NULL") : cr.getString(1);
  string tn = cr.isNull(2) ? string("NULL") : cr.getString(2);
  int rp = cr.getInt(3);

  return SQLiteMaster(t, n, tn, rp);
}

int main(int argc, char** argv)
{
  using std::cout;
  using std::endl;
  
  Data::SQLiteConnection cn("test.s3db");
  cn.open();

  cn.execute("create table if not exists Test (Id integer not null primary key autoincrement, Name varchar(256) not null unique);");
  
  Data::SQLiteCursor cr = cn.cursor("select * from sqlite_master;"); 
  
  // Data::SQLiteDataReader<SQLiteMaster> dr = cr.dataReader<SQLiteMaster>(fromCursor);
  Data::SQLiteDataReader<SQLiteMaster> dr = Data::SQLiteDataReader<SQLiteMaster>(cr, fromCursor);
  
  while (dr.next())
  {
    if (dr.recordNumber() > 1)
    {
      cout << "----------" << endl;
    }
    cout << "Record:    " << dr.recordNumber() << endl 
         << dr.current();
  }
  
  cn.close();
}
